import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'project.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Sizer(
      builder:  (context, orientation, deviceType){
        return MaterialApp(
          title: '',
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: Project(),
        );
      },

    );
  }
}
