import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'projectBloc.dart';
import 'projectState.dart';
class Project extends StatefulWidget {
  @override
  _ProjectState createState() => _ProjectState();
}
class _ProjectState extends State<Project> {
  ProjectBloc _bloc = ProjectBloc();  ///об'явили об'єкт (_bloc) класу ProjectBloc, він поки що "null"

  @override
  void initState() {      ///тільки сторінка народиться, зразу і народиться все в цьому методі
    _bloc = ProjectBloc(); /// народився об'єкт (_bloc) класу ProjectBloc
    super.initState();
  }

  @override
  void dispose() {      ///тільки сторінка закриється, і закриється все в цьому методі, щоб не використовувалося даремно в оперативній пам'яті
    _bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(children: [
          SizedBox(height: 70.0.sp,),
            Row(mainAxisAlignment: MainAxisAlignment.center,
              children: [
                StreamBuilder(              ///тут StreamBuilder це та частина кода, яка перемальовується;
                  stream: _bloc.streamPage,///другий кінець стріма,\\\просто який стрім з блока використовуємо
                  builder: (BuildContext context, snapshot) {///будує(перемальоаує сам віджет)     ///snapshot - результат роботи стріма
                    if (snapshot.data is ProjectImageState) {/// ///якщо результат стріма == дата класу в стейті
                      ProjectImageState state =  snapshot.data as ProjectImageState;///то створюємо об'єкт стейта і надаємо йому результат стріма, як дата клас у стейті
                      return Container(width: 40.0.w,  height: 20.0.h,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Colors.purple, width: 5.0.sp)),
                        child: Image.asset(
                            state.imageFromState, ///imageFromState перемінна, яка прийшла з projectState.dart і буде мінятися на сторінці (кіт/собака)

                          fit: BoxFit.contain)
                      );
                    } else {
                      return Container(
                        width: 40.0.w, height: 20.0.h,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.purple, width: 5.0.sp)),
                        child: Text("Нажми на кнопку, і буде картинка",
                          style: TextStyle(fontSize: 20.0.sp),
                        ),); }},),
                InkWell(
                  onTap: () {
                    _bloc.changeImage();
                    },
                  child: Container(
                    margin: EdgeInsets.only(left: 50.0.sp),
                    width: 50.0.sp,   height: 50.0.sp,
                    decoration: BoxDecoration(color: Colors.grey,
                        borderRadius: BorderRadius.circular(100.0.sp),),),),],
            ),
          SizedBox(height: 5.0.h,),
          Row(mainAxisAlignment: MainAxisAlignment.center,
            children: [
            SizedBox(height: 10.0.sp,),
            StreamBuilder(
              stream: _bloc.streamPage2,
              builder: (BuildContext context, snapshot){
                if(snapshot.data is ProjectColorState){
                  ProjectColorState state =
                      snapshot.data as ProjectColorState;

                   Color pereminnaColor = Colors.red;
                  if (state.stanFromState == true) {
                    pereminnaColor = Colors.red;
                  } else {
                    pereminnaColor = Colors.blueAccent;
                  }
                  return Container(width: 40.0.w,  height: 20.0.h,
                    decoration: BoxDecoration(
                      color: pereminnaColor,
                      border: Border.all(
                          color: Colors.purple, width: 5.0.sp),),);;
                  }else{
                  return Container(width: 40.0.w,  height: 20.0.h,
                    decoration: BoxDecoration(
                      color: Colors.green,
                      border: Border.all(
                          color: Colors.purple, width: 5.0.sp),),);}}),
            InkWell(
              onTap: () {
                _bloc.changeColor();
              },
              child: Container(
                margin: EdgeInsets.only(left: 50.0.sp),
                width: 50.0.sp,   height: 50.0.sp,
                decoration: BoxDecoration(color: Colors.grey,
                  borderRadius: BorderRadius.circular(100.0.sp),),),),],),
        ],
        ),
      ),);
      }
  }
