class ProjectState {
  ProjectState();

  factory ProjectState.komandaOtpravkiDannuxPhoto(String pereminnaBydJaka) = ProjectImageState;
  /// цю команду (.komandaOtpravkiDannuxPhoto(її назва)) тут ми приймаємо з блока.
  /// (String pereminnaBydJaka) в дужках просто пишемо, що ми стрінговську (String) приймаємо змінну, пофігу як називати, тільки тут використовуємо.
  /// І предаємо в клас  (ProjectImageState (назва класу)), який створюємо нижче.
  factory ProjectState.komandaOtpravkiDannuxColor(bool pepeminnaDovilna) = ProjectColorState;
}
/// клас               /// конструктор ((this.))
class ProjectImageState extends ProjectState {    /// тут ми створюємо клас (ProjectImageState(його назва))
  final String imageFromState;                    /// Тут створюємо перемінну типу String, якій передається значення перемінної,
                                                  /// яка прийшла з блока (imageCatDog)
  ProjectImageState(this.imageFromState);         /// ProjectImageState це те, що ми на сторінку передаємо, клас,
                                                  /// який несе в собі перемінну "imageFromState"
}

class ProjectColorState extends ProjectState {
  final bool stanFromState;

  ProjectColorState(this.stanFromState);
}