import 'dart:async';              /// імпорт, бо ми використовуємо асинхронне програмування в блоці
import 'projectState.dart';



///створюємо блок
class ProjectBloc {
                                             ///<ProjectState> які дані будуть рухатися потоком
  final _nazvaStrimaChangePhoto = StreamController<ProjectState>();




                 ///get streamPage ципляємо потік до StreamBuilder
  Stream<ProjectState> get streamPage => _nazvaStrimaChangePhoto.stream; /// streamPage використовуэмо в StreamBuilder

  String imageCatDog = "assets/images/Cat.png"; ///тут заводимо перемінну String і присвоюємо їй шлях до картинки
  bool stanPhotoFromBloc = true;                ///тут заводимо перемінну (stanPhotoFromBloc) tru/false

  final _nazvaStrimaChangeColor = StreamController<ProjectState>();

  Stream<ProjectState> get streamPage2=>_nazvaStrimaChangeColor.stream;
  bool stanColorFromBloc = true;

  void dispose() {
    _nazvaStrimaChangePhoto.close();
    _nazvaStrimaChangeColor.close();
  }

  ///нижче метод
  Future<void> changeImage() async {      /// цей метод (changeImage) використовуємо  в кнопці InkWell (57 строка сторінки)
    if (stanPhotoFromBloc == true) {
      stanPhotoFromBloc = false;
      imageCatDog = "assets/images/Cat.png";
    } else {
      stanPhotoFromBloc = true;
      imageCatDog = "assets/images/Dog.png";
    }
    if (!_nazvaStrimaChangePhoto.isClosed) {            /// Якщо стрім _nazvaStrimaChangePhoto (назва стріма) не закритий (знак ! в умові)
      _nazvaStrimaChangePhoto.sink.add                  /// то сформуй потік (.sink) і додай до нього (.add) команду
      (ProjectState.komandaOtpravkiDannuxPhoto          /// (.komandaOtpravkiDannuxPhoto (назва команди)), яка по ньому піде до
                                                        /// файла (ProjectState. (projectState.dart, в імпорті))
        (imageCatDog));                                 /// і перенесе з собою значення  ((imageCatDog))
    }
  }

  Future<void> changeColor() async {
    if (stanColorFromBloc == true) {
      stanColorFromBloc = false;
    } else {
      stanColorFromBloc = true;
    }

    if (!_nazvaStrimaChangeColor.isClosed) {
      _nazvaStrimaChangeColor.sink.add(ProjectState.komandaOtpravkiDannuxColor(stanColorFromBloc));
    }
  }

}